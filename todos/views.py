from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Todos overview list
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/list.html", context)


# Todo Details page
def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)

    context = {
        "todo_list_detail": todo_list,
    }
    return render(request, "todos/detail.html", context)


# Create Todo
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        # Validate Form
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


# Edit Todo
def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


# Delete Todo
def todo_list_delete(request, id):
    if request.method == "POST":
        list = TodoList.objects.get(id=id)
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


# Todo Item Create
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        # Validate Form
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todo_item/create.html", context)


# Edit Todo Item
def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form,
    }
    return render(request, "todo_item/edit.html", context)
